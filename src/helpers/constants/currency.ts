export const AVAILABLE_CURRENCYS = [
  "UAH",
  "EUR",
  "USD",
  "JPY",
  "GBP",
  "CHF",
  "CAD",
  "ZAR",
] as const;
