//this function limits the number after point to 3.
//if api returns null or user write a letters on incorrect symbols the function will return '0' to display it in input
//if use past in input some letters - function returns only the first group of numbers

export function parseExchangeValue(value: number | string): string {
  const firstNumbers = String(value).match(/[0-9]+/)
  if (value === null) return "0";
  if(isNaN(+value) && firstNumbers) return firstNumbers[0]
  if(isNaN(+value)) return "0"
  if (Number.isInteger(+value)) return `${value}`;
  return parseFloat(String((+value).toFixed(3))).toString();
}
