export interface ICurrencyExchangeItem {
  succes: boolean;
  query: {
    from: string;
    to: string;
    amount: number;
  };
  info: {
    timestamp: number;
    rate: number;
  };
  date: string;
  result: number;
}

export interface ExchangeInfo {
  from: string;
  fromValue: string;
  to: string;
  toValue: string;
  date: string;
}
