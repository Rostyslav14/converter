import { ChangeEvent, FC, useEffect, useState } from "react";
import style from "../styles/homePage.module.scss";
import { Header } from "../components/layout/Header/Header";
import { exchangeService } from "../services/exchange.service";
import { ICurrencyExchangeItem } from "../helpers/types/currencyExchange";
import { Col, Row } from "antd";
import { AVAILABLE_CURRENCYS } from "../helpers/constants/currency";
import { TitleBlock } from "../components/home/TitleBlock/TItleBlock";
import { Loader } from "../components/shared/Loader/Loader";
import { CurrencySelect } from "../components/home/CurrencySelect/CurrencySelect";
import { parseExchangeValue } from "../helpers/parsers/exchange";
interface IProps {}

interface ISelectedCurrencies {
  currency: string;
  value: string;
}

export const Home: FC<IProps> = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [lastUpdateDate,setLastUpdateDate] = useState<string>();
  const [previewCurrencies, setPreviewCurrencies] = useState<ICurrencyExchangeItem[]>();
  const [from, setFrom] = useState<ISelectedCurrencies>({ currency: "EUR", value: "" });
  const [to, setTo] = useState<ISelectedCurrencies>({ currency: "UAH", value: "" });

  useEffect(() => {
    const allPreviewPromises = [];
    for (let el of AVAILABLE_CURRENCYS) {
      if (el !== "UAH") {
        allPreviewPromises.push(
          exchangeService.getCurrentExchange("1", el, "UAH")
        );
      }
    }
    Promise.all(allPreviewPromises)
      .then((data) => setPreviewCurrencies(data))
      .catch((err) => console.error(err))
      .finally(() => setIsLoading(false));
  }, []);

  const resetDisplayedValues = (): void => {
    setTo({ ...to, value: "" });
    setFrom({ ...from, value: "" });
    setLastUpdateDate("");
  };

  const onSelectFromCurrency = (selected: string) => {
    exchangeService
      .getCurrentExchange(from.value, selected, to.currency)
      .then((data) => {
        setLastUpdateDate(data.date);
        setFrom({ ...from, currency: selected });
        setTo({ ...to, value: parseExchangeValue(data.result) });
      })
      .catch((err) => console.error(err));
  };
  const onSelectToCurrency = (selected: string) => {
    exchangeService
      .getCurrentExchange(from.value, from.currency, selected)
      .then((data) => {
        setLastUpdateDate(data.date);
        setTo({ currency: selected, value: parseExchangeValue(data.result) });
      })
      .catch((err) => console.error(err));
  };

  const onEnterFromValue = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length < 1) {
      resetDisplayedValues();
    } else {
      setFrom({ ...from, value: e.target.value });
      exchangeService
        .getCurrentExchange(e.target.value, from.currency, to.currency)
        .then((data) => {
          setLastUpdateDate(data.date);
          setTo({ ...to, value: parseExchangeValue(data.result) });
        })
        .catch((err) => console.error(err));
    }
  };

  const onEnterToValue = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length < 1) {
      resetDisplayedValues();
    } else {
      setTo({ ...to, value: e.target.value });
      exchangeService
        .getCurrentExchange(e.target.value, to.currency, from.currency)
        .then((data) => {
          setLastUpdateDate(data.date);
          setFrom({ ...from, value: parseExchangeValue(data.result) });
        })
        .catch((err) => console.error(err));
    }
  };

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <Header preview={previewCurrencies as ICurrencyExchangeItem[]} />
          <main className={style.main}>
            <div className={style.container + " global-width-limiter"}>
              <Col span={10} className={style.currencyExchangeContainer}>
                <TitleBlock
                  from={from.currency}
                  fromValue={from.value}
                  to={to.currency}
                  toValue={to.value}
                  date={lastUpdateDate}
                />
                <Row>
                  <CurrencySelect
                    onSelect={onSelectFromCurrency}
                    onEnterValue={onEnterFromValue}
                    value={from.value}
                    currency={from.currency}
                  />
                </Row>
                <Row>
                  <CurrencySelect
                    onSelect={onSelectToCurrency}
                    onEnterValue={onEnterToValue}
                    value={to.value}
                    currency={to.currency}
                  />
                </Row>
              </Col>
            </div>
          </main>
        </>
      )}
    </>
  );
};
