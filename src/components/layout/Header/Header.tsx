import { FC } from "react";
import style from "./header.module.scss";
import { ICurrencyExchangeItem } from "../../../helpers/types/currencyExchange";
import CurrencyFlag from "react-currency-flags";

interface IProps {
  preview: ICurrencyExchangeItem[];
}

export const Header: FC<IProps> = (props) => {
  const { preview } = props;
  return (
    <header className={style.header}>
      <div className={style.wrapper + " global-width-limiter"}>
        <h4 className={style.title}>current exchange rate:</h4>
        <ul className={style.previewList}>
          {preview?.map((el) => (
            <li key={el.query.from} className={style.previewListItem}>
              <div className={style.listItemBottom}>
                <strong className={style.currencyName}>
                  1 {el.query.from}
                </strong>
                <CurrencyFlag currency={el.query.from} size="sm" />
              </div>
              <div className={style.listItemBottom}>
                <CurrencyFlag currency={el.query.to} size="sm" />
                <span className={style.previewName}>{el.query.to}</span>
                <span className={style.convertToCurrency}>
                  {el.result.toFixed(2)}
                </span>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
};
