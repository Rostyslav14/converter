import { FC } from "react";
import style from "./loader.module.scss";

interface IProps {}

export const Loader: FC<IProps> = (props) => {
  return (
    <div className={style.wrapper}>
      <h2>Loading...</h2>
    </div>
  );
};
