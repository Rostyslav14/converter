import { Input, Select } from "antd";
import { AVAILABLE_CURRENCYS } from "../../../helpers/constants/currency";
import { ChangeEvent, FC } from "react";

const { Option } = Select;

interface IProps {
  onSelect: (value: string) => void;
  onEnterValue:(e: ChangeEvent<HTMLInputElement>)=>void
  value: string;
  currency: string;
}

export const CurrencySelect: FC<IProps> = (props) => {
  const { onSelect,onEnterValue, value, currency } = props;
  return (
    <>
      <Select
        style={{ width: "35%" }}
        onSelect={(currency) => onSelect(currency)}
        defaultValue={currency}
      >
        {AVAILABLE_CURRENCYS.map((el) => (
          <Option key={el} value={el}>
            {el}
          </Option>
        ))}
      </Select>
      <Input onChange={onEnterValue} value={value} style={{ width: "65%" }} />
    </>
  );
};
