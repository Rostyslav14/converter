import { FC } from "react";
import style from "./titleBlock.module.scss";
import { ExchangeInfo } from "../../../helpers/types/currencyExchange";
import { parseExchangeValue } from "../../../helpers/parsers/exchange";

export const TitleBlock: FC<Partial<ExchangeInfo>> = (props) => {
  const { date, fromValue, toValue, from, to } = props;
  return (
    <div className={style.container}>
      <h5 className={style.additionalHeader}>
        {fromValue ? parseExchangeValue(fromValue) : "0"} {from} is equal
      </h5>
      <h1 className={style.header}>
        {toValue ? parseExchangeValue(toValue) : "0"} {to}
      </h1>
      <div className={style.dateBlock}>
        {date && (
          <>
            <span className={style.dateHeader}>
              Date of the last update of the currency value:
            </span>
            <span className={style.dateValue}>{date}</span>
          </>
        )}
      </div>
    </div>
  );
};
