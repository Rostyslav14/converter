import { config } from "../config";

export const exchangeService = {
  getCurrentExchange,
};

function getCurrentExchange(
  fromAmount: string,
  changeFrom: string,
  changeTo: string
) {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}&apikey=${config.apiKey}&from=${changeFrom}&to=${changeTo}&amount=${fromAmount}`,
    requestOptions
  )
    .then((data) => data.json())
    .catch((err) => console.error(err));
}
